# https://realpython.com/python-sockets/#echo-client

import socket

HOST = '192.168.1.101'  # The server's hostname or IP address
PORT = 8000             # The port used by the server

# Original:
# with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s
# isn't supported in micropython so I changed it to:

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.connect((HOST, PORT))
    s.sendall(b'Hello from ESP')
    data = s.recv(1024)
finally:
    print('Received ', str(data, 'utf8'))

# I think this doesn't close the socket the right way but it works.