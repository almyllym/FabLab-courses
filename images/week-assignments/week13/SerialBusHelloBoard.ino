// Serial BUS example with switching Tx mode

// Orgianal code from our course's moodle page.
// I just added the ledState to toggle the led.
 
#include <SoftwareSerial.h>
#define txPin 1     // transmit signal to the bridge
#define rxPin 3     // recieves signal from bridge 

SoftwareSerial mySerial(rxPin, txPin); // RX, TX

const char node = '3'; // network address
const int ledPin =  2; // the number of the LED pin
bool ledState = 0;

int incomingByte;

void setup() {
  mySerial.begin(9600);
  pinMode(ledPin, OUTPUT);
  pinMode(txPin, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {
    digitalWrite(ledPin, LOW);
    delay(200);
    digitalWrite(ledPin, HIGH);
    delay(200);
    incomingByte = mySerial.read();
    if (incomingByte == node) {    
      pinMode(txPin, OUTPUT); // open line to write
      mySerial.print("node ");  
      mySerial.println(node);  
      pinMode(txPin, INPUT);
      delay(200);
      ledState = !ledState;
      digitalWrite(ledPin, ledState);
    }
  }
}
