# https://realpython.com/python-sockets/#echo-server

import socket

HOST = '192.168.1.101'  # Standard loopback interface address (localhost)
PORT = 8000             # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        while True:
            data = conn.recv(1024)
            print(str(data, 'utf8'))
            if not data:
                break
            conn.sendall(b'Hi from PC! I received: ' + data)