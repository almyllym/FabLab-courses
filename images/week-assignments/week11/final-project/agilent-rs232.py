#!/usr/bin/python3
  
# MIT License

# Copyright (c) 2020 kiwih

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# https://github.com/kiwih/agilent-rs232

# default arguments and serial handshaking changed by Aleksi Myllymäki 2021

import serial
import matplotlib.pyplot as plt
import argparse

import tracemalloc

tracemalloc.start()

# Initiate the parser
parser = argparse.ArgumentParser()

#defaults
port = "COM6"
baud = 9600
channel = 1
length = "100"

# Add arguments
parser.add_argument("--port", "-p", help="set serial port (default %s)" % port)
parser.add_argument("--baud", "-b", help="set serial baud rate (default %d)" % baud)
parser.add_argument("--channel", "-c", help="set probe channel (default %d)" % channel)
parser.add_argument("--length", "-l", help="number of samples (default %s)" % length)

# Read arguments from the command line
args = parser.parse_args()

# Evaluate given options
if args.port:
    port = args.port
if args.baud:
    baud = int(args.baud)
if args.channel:
    channel = int(args.channel)
if args.length:
    if args.length in ("100", "250", "500", "1000", "2000", "MAXimum"):
        length = args.length
    else:
        print("Invalid length (must be in {100, 250, 500, 1000, 2000, MAXimum}")
        exit(1)
    

# Open serial port using the XON/XOFF handshaking mode and 9600 baud
# A timeout is useful when deciding if a response is "finished"
ser = serial.Serial(port, baud, xonxoff=True, timeout=1)  

# Ensure the scope is awake and talking
ser.write(b'*IDN?\n')
ser.flush() #flush the serial to ensure the write is sent

#let's get the response
scope_idn = ser.readline() 
print(scope_idn[0:7])

if scope_idn[0:7] != b'AGILENT':
    print("Unexpected response from Agilent scope, check your connection and try again")
    ser.close()
    exit()

# ask for data to be formatted as signed WORDs (16 bits, so -32,768 through 32,767)
ser.write(b':WAVEform:FORMat BYTE\n')
ser.write(b':WAVeform:BYTeorder MSBFirst\n')
ser.write(b':WAVeform:UNSigned 1\n')

# ask for 1000 data points
pointsString = ":WAVeform:POINts %s\n" % length
ser.write(pointsString.encode())

#set it to examine channel 1 or channel 2
if channel == 1:
    ser.write(b':WAVeform:SOURce CHANnel1\n') 
else:
    ser.write(b':WAVeform:SOURce CHANnel2\n') 

#let's now read what the oscilloscope is set to
ser.write(b':WAVeform:TYPE?\n')
ser.flush()
scope_read_type = ser.readline()[:-1] #TYPE? returns either NORM, PEAK, or AVER followed by a \n

#load dispay parameters. All of these return "NR3" format, which is a float-type
ser.write(b':WAVeform:XINCrement?\n') 
ser.flush()
scope_x_increment = float(ser.readline())

ser.write(b':WAVeform:XORigin?\n')
ser.flush()
scope_x_origin = float(ser.readline())

ser.write(b':WAVeform:XREFerence?\n')
ser.flush()
scope_x_reference = float(ser.readline())

ser.write(b':WAVeform:YINCrement?\n')
ser.flush()
scope_y_increment = float(ser.readline()) 

ser.write(b':WAVeform:YORigin?\n')
ser.flush()
scope_y_origin = float(ser.readline()) 

ser.write(b':WAVeform:YREFerence?\n')
ser.flush()
scope_y_reference = float(ser.readline())

ser.flush()

# let's now try get the data!
ser.write(b':WAVeform:DATA?\n')
ser.flush()
scope_data_bytes = ser.readline() #the response here is formated preamble,data where the preamble provides the length of the data

#we're done with the scope - be a tidy kiwi, don't forget to close the port
ser.close() 

print("fulldata: ", scope_data_bytes)

print("Oscilloscope mode: ",scope_read_type.decode())

print("X increment (S):", scope_x_increment)
print("X reference (S):", scope_x_reference)
print("X origin (S):", scope_x_origin)

print("Y increment (V):", scope_y_increment)
print("Y reference (V):", scope_y_reference)
print("Y origin (V):", scope_y_origin)

#the preamble is in the format #[length of length of data][length of data],[data]
scope_data_preamble_len = scope_data_bytes[1] - 48 #convert the ASCII digit to an integer
scope_data_len = int(scope_data_bytes[2:2+scope_data_preamble_len]) #the data length in bytes
print("Data length (bytes): ", scope_data_len)

data_points = []
for i in range(0, scope_data_len, 2):
    data_offset = i+scope_data_preamble_len + 2
    #data_point = int.from_bytes(scope_data_bytes[data_offset:data_offset+2], byteorder='big', signed=True)

    #using the formula from the agilent 5000 series programmer's guide reference manual page 595
    # voltage = [(data value - yreference) * yincrement] + yorigin
    data_point_voltage = ((scope_data_bytes[data_offset] - scope_y_reference) * scope_y_increment) + scope_y_origin
    data_points.append(data_point_voltage)
print("\n\n\n")
print(scope_data_bytes[0:20])
print("\n\n\n")
print(data_points)


print("Min (V):", min(data_points))
print("Max (V):", max(data_points))

data_points_times = []
for i in range(0, len(data_points)):
    #using the formula from the agilent 5000 series programmer's guide reference manual page 595
    # time = [(data point number - xreference) * xincrement] + xorigin
    data_point_time = ((i - scope_x_reference) * scope_x_increment) + scope_x_origin
    data_points_times.append(data_point_time)

current, peak = tracemalloc.get_traced_memory()
print(f"Current memory usage is {current / 10**6}MB; Peak was {peak / 10**6}MB")
tracemalloc.stop()

#now we want to graph it
plt.plot(data_points_times, data_points)
plt.title("Oscilloscope capture (mode: "+scope_read_type.decode()+")")
plt.xlabel("Time (S)")
plt.xticks(rotation=45)
plt.ylabel("Voltage (V)")
plt.tight_layout()
plt.show()
