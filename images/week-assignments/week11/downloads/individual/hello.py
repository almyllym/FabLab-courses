# Original code made by Iván Sánchez Milara
# Modified by Aleksi Myllymäki

from tkinter import Tk, Label, Button, StringVar, Frame, Entry
import serial, sys, time

class SensorValues (Frame):
    LABEL_TEXT = "Last value received: {}"
    def __init__(self, master, serial):
        super().__init__(master)
        self.serial = serial
        master.title("Hello sensor")
        master.minsize(640,360)
        self.pack()

        self.label_index = 0
        self.label_text = StringVar()
        self.label_text.set(self.LABEL_TEXT.format("0"))
        self.label = Label(master, textvariable=self.label_text, font=("Arial", 25))
        self.label.pack()

        self.send_string = Entry()
        self.send_string.pack()
        self.send_button = Button(master, text="Send it!", command=self.send)
        self.send_button.pack()
        
        self.close_button = Button(master, text="Close", command=master.quit)
        self.close_button.pack()
    
    def idle (self):
        #
        # idle routine
        #

        ser.flush()
        if ser.inWaiting(): # https://stackoverflow.com/questions/11609994/capturing-serial-data-in-background-process/14320813
            self.value = int(ser.readline())
            print(self.value)
            self.label_text.set(self.LABEL_TEXT.format(str(self.value)))
            self.update() 
        self.master.update_idletasks()
        self.master.after(50, self.idle)
    
    def send (self):
        s = self.send_string.get()
        ser.write(s.encode('utf-8'))
        self.send_string.delete(0, "end")




#
#  check command line arguments
#
if (len(sys.argv) != 2):
    print("command line: hellosensor.py serial_port")
    sys.exit()
port = sys.argv[1]
#
# open serial port
#
ser = serial.Serial(port,115200, writeTimeout=1)
ser.setDTR()

#
# create the interface
#
root = Tk()
my_gui = SensorValues(root, ser)
root.after(200, my_gui.idle)
root.mainloop()