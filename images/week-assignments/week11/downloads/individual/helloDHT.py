import utime, machine, dht

d = dht.DHT11(machine.Pin(13))

while True:
    d.measure()
    print(d.humidity()) # prints to serial as ASCII with default baud 115200

    utime.sleep_ms(1000)