import utime, machine, dht, ssd1306
import uos

uos.dupterm(None, 1) # https://docs.micropython.org/en/latest/esp8266/quickref.html?highlight=dht#uart-serial-bus

uart = machine.UART(0, 115200)
uart.init(115200, bits=8, parity=None, stop=1)

d = dht.DHT11(machine.Pin(13))
i2c = machine.I2C(machine.Pin(14), machine.Pin(2)) # SCL, SDA
oled = ssd1306.SSD1306_I2C(128, 64, i2c)
s = ""

while True:
    oled.fill(0)
    a = uart.readline()
    if a is not None:
        s = a.decode('utf-8')
    oled.text(s, 0, 0, 1)
    oled.show()

    d.measure()
    h = d.humidity()
    
    uart.write(str(h))
    uart.write("\r\n") # had to add carriage return

    utime.sleep_ms(1000)