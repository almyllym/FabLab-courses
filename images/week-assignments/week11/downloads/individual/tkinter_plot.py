# Original code made by Iván Sánchez Milara
# Modified by Aleksi Myllymäki

from tkinter import Tk, Label, Button, StringVar, Frame, Entry, Checkbutton, Variable
import serial, sys, time
import matplotlib.pyplot as plt

class SensorValues (Frame):
    LABEL_TEXT = "Last value received: {}"
    value = []
    
    def __init__(self, master, serial):
        super().__init__(master)
        self.serial = serial
        master.title("Tkinter plot")
        master.minsize(640,360)
        self.pack()

        self.label_index = 0
        self.label_text = StringVar()
        self.label_text.set(self.LABEL_TEXT.format("0"))
        self.label = Label(master, textvariable=self.label_text, font=("Arial", 25))
        self.label.pack()

        self.send_string = Entry(font=("Arial", 25))
        self.send_string.pack()
        self.send_button = Button(master, text="Send it!", command=self.send, font=("Arial", 25))
        self.send_button.pack()
        self.plot_button = Button(master, text="Plot it!", command=self.plot, font=("Arial", 25))
        self.plot_button.pack()
        
        self.close_button = Button(master, text="Close", command=master.quit)
        self.close_button.pack(side = "bottom")
    
    def idle (self):
        #
        # idle routine
        #

        ser.flush()
        #https://stackoverflow.com/questions/11609994/capturing-serial-data-in-background-process/14320813
        if ser.inWaiting():
            self.value.append(int(ser.readline()))
            print(self.value[-1])
            self.label_text.set(self.LABEL_TEXT.format(str(self.value[-1])))
            self.update() 
        self.master.update_idletasks()
        self.master.after(50, self.idle)
        #self.master.after_idle(self.idle)
    
    def send (self):
        s = self.send_string.get()
        ser.write(s.encode('utf-8'))
        self.send_string.delete(0, "end")
    
    def plot (self):
        plt.clf()
        plt.plot(self.value)
        plt.show()
        
        # I tried to plot in the Tkinter window but no matter what I tried 
        # it would always make a new object in the tkinter window and plot in 
        # that. I don't now how the objects are handled so I couldn't get it
        # fixed. Here is one of those examples.

        # # imports
        # from matplotlib.backends.backend_tkagg import (
        # FigureCanvasTkAgg, NavigationToolbar2Tk)
        # # Implement the default Matplotlib key bindings.
        # from matplotlib.backend_bases import key_press_handler
        # from matplotlib.figure import Figure

        # #self.canvas.destroy()
        # self.a.plot(self.value)
        # self.canvas = FigureCanvasTkAgg(self.fig, master=root)
        # #self.canvas.get_tk_widget().pack()
        # self.canvas.show()
    
    

    # maybe useful:
    # https://matplotlib.org/3.1.0/gallery/user_interfaces/embedding_in_tk_sgskip.html
    # https://stackoverflow.com/questions/25498937/embed-a-pyplot-in-a-tkinter-window-and-update-it
    # https://stackoverflow.com/questions/31440167/placing-plot-on-tkinter-main-window-in-python
    # https://stackoverflow.com/questions/30774281/update-matplotlib-plot-in-tkinter-gui





#
#  check command line arguments
#
if (len(sys.argv) != 2):
    print("command line: hellosensor.py serial_port")
    sys.exit()
port = sys.argv[1]
#
# open serial port
#
ser = serial.Serial(port,115200, writeTimeout=1)
ser.setDTR()

#
# create the interface
#
root = Tk()
my_gui = SensorValues(root, ser)
root.after(200, my_gui.idle)
root.mainloop()