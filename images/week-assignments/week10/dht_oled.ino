#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <DHT.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C // Silkscreen claims that the address is 0x78 or 0x7A 
                            // depending of the resistor placement
                            // but I2C scan showed that the real address was 0x3C

#define I2C_SDA 2
#define I2C_SCL 14
#define DHTPIN 13
#define DHTTYPE DHT11

TwoWire I2Cssd = TwoWire();
DHT dht(DHTPIN, DHTTYPE);
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &I2Cssd, OLED_RESET);

void setup() {
  I2Cssd.begin(2, 14);
  
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  dht.begin();
  delay(2000);
}

void loop() {  
  display.clearDisplay();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  
  display.setCursor(0, 0);
  display.println("TMP");
  
  display.setCursor(63, 0);
  display.println("HMD");
  

  display.setTextSize(4);
  display.setCursor(0, 16);
  display.println(dht.readTemperature(), 0);
  display.setCursor(63, 16);
  display.println(dht.readHumidity(), 0);

  display.display(); 
  delay(1000);

  
}
