import utime, ntptime, machine, dht, ssd1306

d = dht.DHT11(machine.Pin(13))
i2c = machine.I2C(machine.Pin(14), machine.Pin(2)) # SCL, SDA
oled = ssd1306.SSD1306_I2C(128, 64, i2c)

local = 2 * 60 * 60     # 2 hours to seconds
updatetime = False  # just a flag for updating time
tmp = "TEMPERATURE: "
hmd = "HUMIDITY: "
t_log = 60          # logging interval in seconds

ntptime.NTP_DELTA = 3155673600 - local
ntptime.settime() # get UTC time from NTP server
now_clock = utime.localtime()
now_log   = utime.localtime()

# open with append so incase of reset I don't delete all previous measurements
f = open("log.csv", "a")
# write header if it doesen't already exist
if (f.readline() != "time, TMP, HMD\n"):
    f.write("time, TMP, HMD\n")
f.close()

d.measure()

while True:
    if ((utime.time() - utime.mktime(now_clock)) >= 1):
        now_clock = utime.localtime()

        d.measure()

        oled.fill(0)
        s_time = "%2d:%02d:%02d" %(now_clock[3], now_clock[4], now_clock[5])
        oled.text(s_time, 0, 0, 1)
        oled.text(tmp, 0, 16, 1)
        oled.text( str(d.temperature()), 8 * len(tmp), 16, 1 )
        oled.text(hmd, 0, 24, 1)
        oled.text( str(d.humidity()), 8 * len(tmp), 24, 1 )
        oled.show()

    if ((utime.time() - utime.mktime(now_log)) >= t_log):
        # I can't use d.measure() here because the sensor doesen't guarantee
        # accuracy if the polling rate is higher than 1 Hz
         
        now_log = utime.localtime()
        # hh:mm:ss, TMP, HMD
        s = "%2d:%02d:%02d, %d, %d" %(now_log[3], now_log[4], now_log[5],
                                      d.temperature(), d.humidity())
        
        f = open("log.csv", "a")
        f.write(s + '\n')
        f.close()

    # if hours is dividable with six, update time from server.
    # ESP8266 clock can drift seconds per minute
    if (now_log[3] % 6 == 0 and updatetime == True):
        ntptime.settime()
        updatetime = False
    elif (now_log[3] % 6 != 0):
        updatetime = True