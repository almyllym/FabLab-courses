
// ESP8266
const int in  = D1; // nodeMCU
const int out = D2; // nodeMCU


/*
// ATtiny412
const int in  = 2;
const int out = 3; 
*/

/*
// STM32
const int in  = 7;
const int out = 8; 
*/

void setup() {
  pinMode(in, INPUT);
  pinMode(out, OUTPUT);

  digitalWrite(out, HIGH);
}

void loop() {
  // ESP8266 tries to do WiFi stuff at the start of each void loop() 
  // this while loop prevents that happening
  while (1){ 
    digitalWrite(out, !digitalRead(in));  
  }
  
}
