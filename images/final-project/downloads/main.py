# MIT License

# Copyright (c) 2021 Aleksi Myllymäki
# Copyright (c) 2020 kiwih

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from time import sleep
from umqtt.simple import MQTTClient
from machine import UART
from machine import Pin

# import gc
# import micropython
# gc.collect()
# micropython.mem_info()
device = '54622D'
raw_in = bytes(f"{device}/raw_in", 'utf8')
raw_out = bytes(f"{device}/raw_out", 'utf8')
volt_out = bytes(f"{device}/voltage", 'utf8')
time_out = bytes(f"{device}/time", 'utf8')
get1 = bytes(f"{device}/get1", 'utf8')
server = '192.168.1.108'
client = device
mqtt = MQTTClient(client, server)

uart2 = UART(2, baudrate=9600, tx=17, rx=16, bits=8, parity=None, stop=1, txbuf=0, rxbuf=1000)
button = Pin(4, Pin.IN, Pin.PULL_UP)

def getData(uart, channel, length):
    uart.write(b':SYSTem:DSP "Capturing and sending data..."\n')
    uart.write(b'*IDN?\n')
    uart_wait(uart)
    idn = uart.readline()
    if idn[0:7] != b'AGILENT':
        print('Device not recognised')
    
    uart.write(b':WAVEform:FORMat BYTE\n')
    uart_wait(uart)
    uart.write(b':WAVeform:BYTeorder MSBFirst\n')
    uart_wait(uart)
    uart.write(b':WAVeform:UNSigned 1\n')
    uart_wait(uart)

    uart.write(bytes(f":WAVeform:POINts {length}\n", 'utf8'))

    if channel == 1:
        uart.write(b':WAVeform:SOURce CHANnel1\n')
    else:
        uart.write(b':WAVeform:SOURce CHANnel2\n')
    
    if channel == 1:
        uart.write(b':DIGitize CHANnel1\n')
    else:
        uart.write(b':DIGitize CHANnel2\n')

    uart.write(b':WAVeform:TYPE?\n')
    uart_wait(uart)
    scope_read_type = uart.readline()[:-1] 

    uart.write(b':WAVeform:XINCrement?\n') 
    uart_wait(uart)
    scope_x_increment = float(uart.readline())

    uart.write(b':WAVeform:XORigin?\n')
    uart_wait(uart)
    scope_x_origin = float(uart.readline())

    uart.write(b':WAVeform:XREFerence?\n')
    uart_wait(uart)
    scope_x_reference = float(uart.readline())

    uart.write(b':WAVeform:YINCrement?\n')
    uart_wait(uart)
    scope_y_increment = float(uart.readline()) 

    uart.write(b':WAVeform:YORigin?\n')
    uart_wait(uart)
    scope_y_origin = float(uart.readline()) 

    uart.write(b':WAVeform:YREFerence?\n')
    uart_wait(uart)
    scope_y_reference = float(uart.readline())

    uart.write(b':WAVeform:DATA?\n')
    sleep(0.5) # I have no idea why uart_wait didn't work here
    scope_data_bytes = uart.readline()
    print(scope_data_bytes)

    print("X increment (S):", scope_x_increment)
    print("X reference (S):", scope_x_reference)
    print("X origin (S):", scope_x_origin)

    print("Y increment (V):", scope_y_increment)
    print("Y reference (V):", scope_y_reference)
    print("Y origin (V):", scope_y_origin)

    scope_data_preamble_len = scope_data_bytes[1] - 48 #convert the ASCII digit to an integer
    scope_data_len = int(scope_data_bytes[2:2+scope_data_preamble_len]) #the data length in bytes
    print("Data length (bytes): ", scope_data_len)
    
    print(len(scope_data_bytes))


    # gc.collect()
    # print('Initial free: {} allocated: {}'.format(gc.mem_free(), gc.mem_alloc()))
    print(scope_data_bytes[0:scope_data_preamble_len+2])
    data_points = []
    for i in range(0, scope_data_len, 1):
        #gc.collect()
        #print('{} Initial free: {} allocated: {}'.format(i, gc.mem_free(), gc.mem_alloc()))
        data_offset = i+scope_data_preamble_len + 2
        
        #data_point = int.from_bytes(scope_data_bytes[data_offset:data_offset+2], 'big', True)
        print(scope_data_bytes[data_offset])
        #using the formula from the agilent 5000 series programmer's guide reference manual page 595
        # voltage = [(data value - yreference) * yincrement] + yorigin
        data_point_voltage = ((scope_data_bytes[data_offset] - scope_y_reference) * scope_y_increment) + scope_y_origin
        data_points.append(data_point_voltage)
    print(data_points)
    mqtt.publish(volt_out, f'{data_points}')

    # print("Min (V):", min(data_points))
    # print("Max (V):", max(data_points))

    data_points_times = []
    for i in range(0, len(data_points)):
        #using the formula from the agilent 5000 series programmer's guide reference manual page 595
        # time = [(data point number - xreference) * xincrement] + xorigin
        data_point_time = ((i - scope_x_reference) * scope_x_increment) + scope_x_origin
        data_points_times.append(data_point_time)
    mqtt.publish(time_out, f'{data_points_times}')
    uart.write(b':SYSTem:DSP ""\n')


def sub_cb(topic, msg):
    if topic.decode('utf8').split('/')[1] == "raw_in":
        print(topic, msg.decode('utf8').replace('\\n', '\n'))
        uart2.write(msg.decode('utf8').replace('\\n', '\n'))
        uart_wait(uart2)
        answer = uart2.readline()
        print(answer)
        if answer != None:
            mqtt.publish(raw_out, answer)
    if topic.decode('utf8').split('/')[1] == "get1":
        getData(uart2, 1, "250")


def uart_wait(uart):
    for i in range(10):
        if uart.any() != 0:
            break
        sleep(0.2)


def main():
    mqtt.set_callback(sub_cb)
    mqtt.connect()
    mqtt.subscribe(raw_in)
    mqtt.subscribe(get1)
    while True:
        # Non-blocking wait for message
        print("check messages")
        mqtt.check_msg()

        if button.value() == 0:
            getData(uart2, 1, "250")

        # Then need to sleep to avoid 100% CPU usage (in a real
        # app other useful actions would be performed instead)
        sleep(0.1)


if __name__ == "__main__":
    main()