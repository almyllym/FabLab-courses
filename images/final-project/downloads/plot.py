from paho.mqtt import client as mqtt_client
import random
import matplotlib.pyplot as plt

broker ='192.168.1.108'
port = 1883

device = '54622D'
raw_out = f"{device}/raw_in"
volt_in = f"{device}/voltage"
time_in = f"{device}/time"

client_id = f'python-mqtt-{random.randint(0, 1000)}'

time = []
voltage = []

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        # print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
        # print(msg.payload.decode('unicode-escape'))
        print('New data incoming')

    client.subscribe(volt_in)
    client.subscribe(time_in)
    client.on_message = on_message


def on_message_volt(client, userdata, msg):
    #print(msg)
    global voltage
    voltage = str(msg.payload.decode()).strip('[]').split(', ')
    voltage = [float(i) for i in voltage] #https://stackoverflow.com/questions/1614236/in-python-how-do-i-convert-all-of-the-items-in-a-list-to-floats
    # print(voltage)


def on_message_time(client, userdata, msg):
    #print(msg)
    global time
    time = str(msg.payload.decode()).strip('[]').split(', ')
    time = [float(i) for i in time]
    # print(time)
    # plt.ion()
    print("Save and close the figure to get a new capture")
    plt.plot(time, voltage)
    plt.tight_layout()
    #plt.pause(0.001)
    plt.show()
    # plt.pause(1)
    # plt.ioff()
    # input("press enter")

def run():
    client = connect_mqtt()
    
    subscribe(client)
    client.message_callback_add(volt_in, on_message_volt)
    client.message_callback_add(time_in, on_message_time)
    client.loop_forever()
    print("testi")


if __name__ == '__main__':
    run()